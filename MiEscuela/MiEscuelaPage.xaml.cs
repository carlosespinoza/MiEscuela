﻿using System.Linq;
using MiEscuela.DAL;
using MiEscuela.Entidades;
using Xamarin.Forms;

namespace MiEscuela
{
    public partial class MiEscuelaPage : ContentPage
    {
        RepositorioGenerico<Usuario> repositorio;
        public MiEscuelaPage()
        {
            InitializeComponent();
            repositorio = new RepositorioGenerico<Usuario>();
            contenedor.BindingContext = new Usuario();
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            if(repositorio.Create(contenedor.BindingContext 
                                  as Usuario)!=null){
                DisplayAlert("Mi escuela","Usuario creado","Ok");
            }else{
                DisplayAlert("Error",repositorio.Error,"Ok");
            }
        }

        void Ingresar_Clicked(object sender, System.EventArgs e)
        {
            Usuario datos = contenedor.BindingContext as Usuario;
            Usuario r = repositorio.Query(v => v.NombreUsuario == datos.NombreUsuario && v.Password == datos.Password).SingleOrDefault();
            if(r!=null){
                DisplayAlert("Bienvenido", r.NombreUsuario, "Ok");
                Navigation.PushAsync(new Vistas.Escuela(r));
                Navigation.RemovePage(this);
            }else{
                DisplayAlert("Error","Nombre de usuario o contraseña incorrecto", "Ok");
            }
        }
    }
}
