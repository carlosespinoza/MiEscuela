﻿using System;
using System.Linq;
using System.Linq.Expressions;
using MiEscuela.Entidades;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MiEscuela.DAL
{
    public class RepositorioGenerico<T> where T:BaseDTO
    {
        private MongoClient client;
        private IMongoDatabase db;
        public string Error { get; private set; }
        public RepositorioGenerico()
        {
            client = new MongoClient(new MongoUrl("mongodb://usuariocompartido:iteshu2018mixta@ds028559.mlab.com:28559/miescueladb"));
            db = client.GetDatabase("miescueladb");
        }

        private IMongoCollection<T> Collection()
        {
            return db.GetCollection<T>(typeof(T).Name);
        }
        public T Create(T entidad)
        {
            entidad.Id = ObjectId.GenerateNewId();
            try{
                Collection().InsertOne(entidad);
                Error = "";
                return entidad;

            }catch(Exception ex){
                Error = ex.Message;
                return null;
            }
        }
        public bool Delete(ObjectId id)
        {
            try
            {
                int r = (int)Collection().DeleteOne(v => v.Id == id).DeletedCount;
                Error = "";
                return r > 0;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        public T Update(T entidad)
        {
            try
            {
                int r = (int)Collection().ReplaceOne(v => v.Id == entidad.Id, entidad).ModifiedCount;
                Error = "";
                return r > 0 ? entidad : null;

            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
        public IQueryable<T> Read => Collection().AsQueryable();

        public T SearchById(ObjectId id){
            return Collection().Find(v => v.Id == id).SingleOrDefault();
        }
        public IQueryable<T> Query(Expression<Func<T,bool>> predicado){
            return Collection().Find(predicado).ToList().AsQueryable();
        }
    }
}
