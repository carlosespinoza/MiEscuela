﻿using System;
using MongoDB.Bson;

namespace MiEscuela.Entidades
{
    public class Amigo:BaseDTO
    {
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public ObjectId IdUsuario { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1}", Nombre, Apellidos);
        }
    }
}
