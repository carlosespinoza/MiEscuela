﻿using System;
using MongoDB.Bson;

namespace MiEscuela.Entidades
{
    public class Materia:BaseDTO
    {
        public string Horario { get; set; }
        public string Profesor { get; set; }
        public ObjectId IdUsuario { get; set; }
        public string Nombre { get; set; }
        public override string ToString()
        {
            return Nombre;
        
        }
    }
}
