﻿using System;
using MongoDB.Bson;

namespace MiEscuela.Entidades
{
    public abstract class BaseDTO:IDisposable
    {
        public ObjectId Id { get; set; }
        private bool isDiposed;
        public void Dispose(){
            this.isDiposed = true;
            GC.SuppressFinalize(this);
        }
    }
}
