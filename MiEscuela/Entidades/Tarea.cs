﻿using System;
using MongoDB.Bson;

namespace MiEscuela.Entidades
{
    public class Tarea:BaseDTO
    {
        public string Titulo { get; set; }
        public DateTime FechaEntrega { get; set; }
        public bool Entregada { get; set; }
        public string Descripcion { get; set; }
        public ObjectId IdMateria { get; set; }

        public override string ToString()
        {
            string texto = Titulo + " ";
            texto += Entregada ? "Entregada" : FechaEntrega.ToString();
            return texto;
        }
    }
}
