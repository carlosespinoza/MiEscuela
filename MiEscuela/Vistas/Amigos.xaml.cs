﻿using System;
using System.Collections.Generic;
using MiEscuela.DAL;
using MiEscuela.Entidades;
using Xamarin.Forms;

namespace MiEscuela.Vistas
{
    public partial class Amigos : ContentPage
    {
        bool EsNuevo;
        RepositorioGenerico<Amigo> repositorio;
        public Amigos(Amigo amigo,bool esNuevo)
        {
            InitializeComponent();
            repositorio = new RepositorioGenerico<Amigo>();
            this.Title = esNuevo ? "Nuevo Amigo" : amigo.ToString();
            Contenedor.BindingContext = amigo;
            EsNuevo = esNuevo;
            btnGuardar.Clicked += (sender, e) => {
                if(EsNuevo){
                    if(repositorio.Create(Contenedor.BindingContext as Amigo)!=null){
                        DisplayAlert("Mi Escuela","Amigo creado","Ok");
                        Navigation.PopAsync();
                    }else{
                        DisplayAlert("Mi Escuela","Error al guardar el amigo","Ok");
                    }
                }else{
                    if (repositorio.Update(Contenedor.BindingContext as Amigo) != null)
                    {
                        DisplayAlert("Mi Escuela", "Amigo guardado", "Ok");
                        Navigation.PopAsync();
                    }
                    else
                    {
                        DisplayAlert("Mi Escuela", "Error al guardar el amigo", "Ok");
                    }
                }
            };
            btnEliminar.Clicked += (sender, e) => {
                if(repositorio.Delete((Contenedor.BindingContext as Amigo).Id)){
                    DisplayAlert("Mi Escuela", "Amigo eliminado", "Ok");
                        Navigation.PopAsync();
                    }
                    else
                    {
                        DisplayAlert("Mi Escuela", "Error al eliminar el amigo", "Ok");
                     
                }
            };
        }
    }
}
