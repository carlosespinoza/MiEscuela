﻿using System;
using System.Collections.Generic;
using MiEscuela.DAL;
using MiEscuela.Entidades;
using MongoDB.Bson;
using Xamarin.Forms;

namespace MiEscuela.Vistas
{
    public partial class Tareas : ContentPage
    {
        bool EsNueva;
        Tarea Tarea;
        RepositorioGenerico<Tarea> repositorio;
        public Tareas(Tarea tarea,bool esNueva)
        {
            InitializeComponent();
            repositorio = new RepositorioGenerico<Tarea>();
            EsNueva = esNueva;
            if(esNueva){
                tarea.FechaEntrega = DateTime.Now;
            }
            Tarea = tarea;
            Title = esNueva ? "Nueva Tarea" : tarea.Titulo;
            Contenedor.BindingContext = tarea;
            btnEliminar.Clicked += (sender, e) =>
            {
                //Delegado
                if(repositorio.Delete(tarea.Id)){
                    DisplayAlert("Mensaje","Tarea eliminada","Ok");
                    Navigation.PopAsync();
                }else{
                    DisplayAlert("Error","Error al eliminar la tarea","Ok");
                }
            };
            btnGuardar.Clicked += (sender, e) =>
            {
                if(esNueva){
                    if(repositorio.Create(Contenedor.BindingContext as Tarea)!=null){
                        DisplayAlert("Mi escuela","Tarea Agregada","Ok");
                        Navigation.PopAsync();
                    }else{
                        DisplayAlert("Error","Error al guardar la tarea","Ok");
                    }
                }else{
                    if (repositorio.Update(Contenedor.BindingContext as Tarea) != null)
                    {
                        DisplayAlert("Mi escuela", "Tarea Modificada", "Ok");
                        Navigation.PopAsync();
                    }
                    else
                    {
                        DisplayAlert("Error", "Error al guardar la tarea", "Ok");
                    } 
                }
            };
        }
    }
}
