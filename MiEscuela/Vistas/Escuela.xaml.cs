﻿using System;
using System.Collections.Generic;
using MiEscuela.DAL;
using MiEscuela.Entidades;
using Xamarin.Forms;

namespace MiEscuela.Vistas
{
    public partial class Escuela : ContentPage
    {
        Usuario usuario;
        RepositorioGenerico<Materia> MateriaRepository;
        RepositorioGenerico<Amigo> AmigoRepository;
        public Escuela(Usuario usuario)
        {
            InitializeComponent();
            this.usuario = usuario;
            MateriaRepository = new RepositorioGenerico<Materia>();
            AmigoRepository = new RepositorioGenerico<Amigo>();
            btnAgregarAmigo.Clicked += (sender, e) => { 
                Amigo a = new Amigo();
                a.IdUsuario = usuario.Id;
                Navigation.PushAsync(new Amigos(a, true));
            };
            lstAmigos.ItemTapped += (sender, e) => {
                Navigation.PushAsync(new Amigos(lstAmigos.SelectedItem as Amigo,false));
            };
            btnAgregarMateria.Clicked += (sender,e)=>{
                Navigation.PushAsync(new Materias(new Materia(){
                    IdUsuario=usuario.Id
                },true));
            };
            lstMaterias.ItemTapped += (sender, e) => {
                Navigation.PushAsync(new Materias(lstMaterias.SelectedItem as Materia,false));
            };
        }



        protected override void OnAppearing()
        {
            base.OnAppearing();
            lstAmigos.ItemsSource = null;
            lstAmigos.ItemsSource = AmigoRepository.Query(a => a.IdUsuario == usuario.Id);
            lstMaterias.ItemsSource = null;
            lstMaterias.ItemsSource = MateriaRepository.Query(m => m.IdUsuario == usuario.Id);
        }
    }
}
