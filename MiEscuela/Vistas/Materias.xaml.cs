﻿using System;
using System.Collections.Generic;
using MiEscuela.DAL;
using MiEscuela.Entidades;
using Xamarin.Forms;

namespace MiEscuela.Vistas
{
    public partial class Materias : ContentPage
    {
        bool EsNueva;
        Materia materia;
        RepositorioGenerico<Materia> repositorio;
        RepositorioGenerico<Tarea> repositorioTareas;
        public Materias(Materia m, bool esNueva)
        {
            InitializeComponent();
            EsNueva = esNueva;
            materia = m;
            repositorio = new RepositorioGenerico<Materia>();
            repositorioTareas = new RepositorioGenerico<Tarea>();
            this.Title = esNueva ? "Nueva Materia" : m.Nombre;
            Contenedor.BindingContext = m;
            btnGuardar.Clicked += (sender, e) =>
            {
                if(esNueva){
                    if(repositorio.Create(Contenedor.BindingContext as Materia)!=null){
                        DisplayAlert("Mi Escuela","Materia Guardada","Ok");
                        Navigation.PopAsync();
                    }else{
                        DisplayAlert("Error","Error al guardar el dato","Ok");
                    }
                }else{
                    if (repositorio.Update(Contenedor.BindingContext as Materia) != null)
                    {
                        DisplayAlert("Mi Escuela", "Materia Guardada", "Ok");
                        Navigation.PopAsync();
                    }
                    else
                    {
                        DisplayAlert("Error", "Error al guardar el dato", "Ok");
                    }
                }
            };
            btnEliminar.Clicked += (sender, e) => {
                if(repositorio.Delete((Contenedor.BindingContext as Materia).Id)){
                        DisplayAlert("Mi Escuela", "Materia Eliminada", "Ok");
                        Navigation.PopAsync();
                    }
                    else
                    {
                        DisplayAlert("Error", "Error al eliminar el dato", "Ok");

                }
            };
            btnAgregarTarea.Clicked += (sender, e) =>
            {
                Navigation.PushAsync(new Tareas(new Tarea(){
                    IdMateria=materia.Id
                },true));
            };
            lstTareas.ItemTapped += (sender, e) =>
            {
                Navigation.PushAsync(new Tareas(lstTareas.SelectedItem as Tarea,false));
            };
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            lstTareas.ItemsSource = null;
            lstTareas.ItemsSource = repositorioTareas.Query(t => t.IdMateria == materia.Id);
        }
    }
}
